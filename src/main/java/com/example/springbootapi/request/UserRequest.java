package com.example.springbootapi.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Pattern;

//public class UserRequest {
//    @Pattern(regexp = ".{1,50}")
//    private String name;
//    public String getName() {
//        return name;
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//}

@Getter
@Setter
public class UserRequest {
    @Pattern(regexp = ".{1,50}")
    private String name;
}