package com.example.springbootapi.controller;

import com.example.springbootapi.entity.User;
import com.example.springbootapi.exception.NotFoundException;
import com.example.springbootapi.repository.UserRepository;
import com.example.springbootapi.request.UserRequest;
import com.example.springbootapi.response.ErrorResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserRepository userRepository;

    @GetMapping
    public List<User> getAll(
            @RequestParam(value = "name", required = false) String name) {
        if (!isNull(name)) {
            return userRepository.findByNameContaining(name);
        }
        return userRepository.findByOrderByIdDesc();
    }

    @GetMapping("/{id}")
    public User getOne(@PathVariable("id") Integer id) {

        return userRepository.findById(id)
//        オリジナルのクエリ文
//        さらにブランチ追加
//        return userRepository.findByIdOriginal(id)
                .orElseThrow(() -> new NotFoundException(id + " is not found."));
    }

    @PostMapping
    public User save(@RequestBody @Valid UserRequest request) {
        User entity = new User();
        entity.setName(request.getName());

        return userRepository.save(entity);
    }

    @PutMapping("/{id}")
    public User update(@RequestBody UserRequest request,
                       @PathVariable("id") Integer id) {

        User entity = new User();
        entity.setId(id);
        entity.setName(request.getName());
        return userRepository.saveAndFlush(entity);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id) {
        userRepository.deleteById(id);
    }
}