package com.example.springbootapi.repository;

import com.example.springbootapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    List<User> findByOrderByIdDesc();
    List<User> findByNameContaining(String name);
    @Query("select t from User t where t.id = :id")
    Optional<User> findByIdOriginal(@Param("id") Integer id);
}